from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import seleksi.urls as seleksi

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='seleksi/', permanent = True), name='$'),
    url(r'^seleksi/', include(seleksi,namespace='seleksi')),

]
