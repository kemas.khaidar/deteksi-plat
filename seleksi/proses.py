from skimage.io import imread
from skimage.filters import threshold_otsu
import matplotlib.pyplot as plt
import imutils
import warnings
from skimage import measure
from skimage.measure import regionprops
import matplotlib.patches as patches
import numpy as np
from skimage.transform import resize
import pickle
import os


def prosesImage(image):
    warnings.filterwarnings('ignore')

    car_image = imread(image, as_gray=True)

    gray_car_image = car_image * 255
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(gray_car_image, cmap="gray")
    threshold_value = threshold_otsu(gray_car_image)
    binary_car_image = gray_car_image > threshold_value

    label_image = measure.label(binary_car_image)

    plate_objects_cordinates = list()
    plate_like_objects = list()

    fig, (ax1) = plt.subplots(1)
    ax1.imshow(gray_car_image, cmap="gray")
    for region in regionprops(label_image):
        if region.area < 50:
            continue

        plate_dimensions = [
            (0.03*label_image.shape[0], 0.08*label_image.shape[0],0.15*label_image.shape[1], 0.3*label_image.shape[1]),
            (0.08*label_image.shape[0], 0.2*label_image.shape[0],0.15*label_image.shape[1], 0.4*label_image.shape[1])
        ]

        min_row, min_col, max_row, max_col = region.bbox

        region_height = max_row - min_row
        region_width = max_col - min_col

        for dimension in plate_dimensions:
            min_height, max_height, min_width, max_width = dimension

            if region_height >= min_height and region_height <= max_height and region_width >= min_width and region_width <= max_width and region_width > region_height:
                plate_like_objects.append(binary_car_image[min_row:max_row, min_col:max_col])
                plate_objects_cordinates.append((min_row, min_col, max_row, max_col))
                rectBorder = patches.Rectangle((min_col, min_row), max_col - min_col, max_row - min_row, edgecolor="red", linewidth=2, fill=False)
                ax1.add_patch(rectBorder)

    plt.axis('off')
    plate = 'static/images/result/plate.png'
    if os.path.isfile(plate):
        os.remove(plate)
    plt.savefig(plate, bbox_inches='tight')

    for i in range(len(plate_like_objects)):
        license_plate = plate_like_objects[i]

        labelled_plate = measure.label(license_plate)

        fig, ax1 = plt.subplots(1)
        ax1.imshow(license_plate, cmap="gray")

        character_dimensions = (0.35*license_plate.shape[0], 0.60*license_plate.shape[0], 0.05*license_plate.shape[1], 0.15*license_plate.shape[1])

        min_height, max_height, min_width, max_width = character_dimensions

        characters = []
        counter = 0
        column_list = []
        detected = False

        for regions in regionprops(labelled_plate):
            y0, x0, y1, x1 = regions.bbox
            region_height = y1 - y0
            region_width = x1 - x0

            if region_height > min_height and region_height < max_height and region_width > min_width and region_width < max_width:
                roi = license_plate[y0:y1, x0:x1]

                # draw a red bordered rectangle over the character.
                rect_border = patches.Rectangle((x0, y0), x1 - x0, y1 - y0, edgecolor="red", linewidth=2, fill=False)
                ax1.add_patch(rect_border)

                # resize the characters to 20X20 and then append each character into the characters list
                resized_char = resize(roi, (20, 20))
                characters.append(resized_char)

                # this is just to keep track of the arrangement of the characters
                column_list.append(x0)

                detected = True

        plt.axis('off')

        segmented = 'static/images/result/segmented.png'
        if os.path.isfile(segmented):
            os.remove(segmented)
        plt.savefig(segmented, bbox_inches='tight')

        if not detected:
            fig.delaxes(ax1)

    filename = 'seleksi/predict_model.sav'
    model = pickle.load(open(filename, 'rb'))

    classification_result = []
    for each_character in characters:
        # converts it to a 1D array
        each_character = each_character.reshape(1, -1)
        print(each_character)
        result = model.predict(each_character)
        classification_result.append(result)

    plate_string = ''
    for eachPredict in classification_result:
        plate_string += eachPredict[0]

    column_list_copy = column_list[:]
    column_list.sort()
    rightplate_string = ''
    for each in column_list:
        rightplate_string += plate_string[column_list_copy.index(each)]

    return rightplate_string
