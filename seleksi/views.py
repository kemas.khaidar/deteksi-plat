from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from operator import itemgetter
from .proses import prosesImage
import json

# Create your views here.


def index(request):
    response = {}
    return render(request, 'home.html', response)


def timeline(request,plate):
    response = {}
    response['plate'] = plate
    return render(request, 'timeline.html', response)


@csrf_exempt
def proses(request):
    if request.method == 'POST':
        picture = request.FILES['file']
        plate = prosesImage(picture)
        return JsonResponse({'plate': plate})
