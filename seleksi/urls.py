from django.conf.urls import url
from django.urls import path
from django.views.generic.base import RedirectView
from .views import *

app_name = 'seleksi'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='home/', permanent=False), name='$'),
    url(r'home/', index, name='index'),
    url(r'proses/', proses, name='proses'),
    path('timeline/<plate>/', timeline, name='timeline'),
]
