function readURL(input) {

  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
			$('#blah').show();
      $('#blah').attr('src', e.target.result);
    }

    reader.readAsDataURL(input.files[0]);
    var formData = new FormData();

	formData.append('file', input.files[0]);

	$.ajax({
		url: '/seleksi/proses/',
		type: 'POST',
		data: formData,
		cache: false,
    	contentType: false,
    	processData: false,
		success: function (data) {
			window.location.href = '/seleksi/timeline/' + data.plate + '/';
		}
	});
  }
}

$("#fileupload").change(function() {
  readURL(this);
});